package cz.cvut.fel.omo.trackingSystem;

/**
 * Tracker is device installed into company vehicles, connected to car computer in order to obtain necessary data.
 */
public class Tracker {
    private Vehicle currentVehicle;
    private final int trackerId;
    private int innerMemory;
    // IMPLEMENT ME, PLEASE!
    public Tracker(Vehicle currentVehicle, int trackerId, int innerMemory) {
        this.currentVehicle = currentVehicle;
        this.trackerId = trackerId;
        this.innerMemory = innerMemory;
    }


    public Vehicle getCurrentVehicle() {
        return currentVehicle;
    }

    public void attachVehicle(Vehicle vehicle){
        this.currentVehicle = vehicle;
        resetTrackerMilage();
    }

    public int getTrackerMilage(){
        //This difference in value should determine, just how many kilometers have gone by since we attached the tracker
        return currentVehicle.getMileage() - innerMemory;
    }

    public void resetTrackerMilage(){
        this.innerMemory = currentVehicle.getMileage();
    }

    public String toString(){
        return  "Tracker_" + trackerId + ", attached to " + currentVehicle.toString();
    }
}

package cz.cvut.fel.omo.trackingSystem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuki on 22/09/2017.
 * GpsTrackingSystem class represents the newly introduced tool for gaining control over company car park.
 */
public class GpsTrackingSystem {
    private List<Tracker> trackers;
    private static int counter;

    public GpsTrackingSystem(){
        this.trackers = new ArrayList<>();
        counter = 0;
    }
    public GpsTrackingSystem(List<Tracker> trackers){
        this.trackers = trackers;
        counter = 0;
    }

    public void attachTrackingDevices(List<Vehicle> vehicles){
        for (Vehicle car: vehicles){
            trackers.add(new Tracker(car,counter,car.getMileage()));
            counter++;
        }
    }

    public void generateMonthlyReport(){
        System.out.println("—– GPS Tracking system: Monthly report —–\n" +
                "\n" +
                "Currently active devices:\n");
        int kmCounter = 0;
        for(Tracker tracker:trackers){
            kmCounter+=tracker.getTrackerMilage();
            System.out.println(tracker.toString() + "\n");
            tracker.resetTrackerMilage();
        }
        System.out.println("This month traveled distance: "+ kmCounter +" Km.");
    }



}

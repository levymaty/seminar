package cz.cvut.fel.omo.trackingSystem;

import java.util.stream.Stream;

/**
 * Class Vehicle represents a single car in company car park.
 */
public class Vehicle {
    private int mileage;
    private final String VINCode, manufacturer;
    public Vehicle(String VINCode, String manufacturer, int mileage){
        this.VINCode = VINCode;
        this.manufacturer = manufacturer;
        this.mileage = mileage;
    }
    public void drive(int km){
        mileage+=km;
    }

    public int getMileage() {
        return mileage;
    }

    public String getVINCode() {
        return VINCode;
    }

    public String getManufacturer() {
        return manufacturer;
    }
    public String toString(){
        return manufacturer + ", " + VINCode;
    }
}

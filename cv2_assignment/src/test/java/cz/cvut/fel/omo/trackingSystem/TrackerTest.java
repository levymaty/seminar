package cz.cvut.fel.omo.trackingSystem;

import org.junit.Assert;
import org.junit.Test;


import static org.junit.Assert.*;

/**
 * Created by kuki on 22/09/2017.
 */
public class TrackerTest {
    int innerMemory = Params.innerMemory;
    int trackerId = Params.trackerId;
    Vehicle car = Params.newCar();
    Tracker test = Params.newTracker();

    @Test
    public void getCurrentVehicle_getsTheCarTheVehicleIsAttachedTo_AssertsTrue() {
        assertEquals(car, test.getCurrentVehicle());
    }

    @Test
    public void attachVehicle_changesTheCurrentVehicleAndResetsInnerMemory_AssertsTrue() {
        String temp = Params.manufacturer;
        Params.manufacturer = "STOOL";
        car = Params.newCar();
        test.attachVehicle(car);
        assertEquals(car, test.getCurrentVehicle());
        Params.manufacturer = temp;
    }

    @Test
    public void getTrackerMilage_tellsUsHowManyKilometersHaveGoneBySinceTheTrackerHasBeenReset_AssertsTrue(){
        int km = 20;
        car = new Vehicle(Params.VINCode,Params.manufacturer,Params.mileage);
        test.attachVehicle(car);
        test.getCurrentVehicle().drive(km);
        assertEquals(km, test.getTrackerMilage());
    }

    @Test
    public void resetTrackerMilage() {
        int km = 20;
        car = new Vehicle(Params.VINCode,Params.manufacturer,Params.mileage);
        test.attachVehicle(car);
        test.getCurrentVehicle().drive(km);
        test.resetTrackerMilage();
        assertEquals(0, test.getTrackerMilage());
    }

    @Test
    public void testToString_PresentsTheTrackerAsAString_ReturnsTracker666attachedtosimp69() {
        String expected = "Tracker_" + trackerId + ", attached to " + test.getCurrentVehicle().toString();
        assertEquals(expected, test.toString());
    }
}
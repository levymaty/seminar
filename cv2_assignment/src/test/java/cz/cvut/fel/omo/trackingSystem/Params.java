package cz.cvut.fel.omo.trackingSystem;

public class Params {
    public static int mileage = 0;
    public static String VINCode = "69";
    public static String manufacturer = "SIMP";
    public static int trackerId = 666;
    public static int innerMemory = 0;
    public static Vehicle vehicle = new Vehicle(VINCode,manufacturer,mileage);
    public static Tracker tracker = new Tracker(vehicle,trackerId,innerMemory);

    public static Vehicle newCar(){
        vehicle = new Vehicle(VINCode,manufacturer,mileage);
        return vehicle;
    }

    public static Tracker newTracker(){
        tracker = new Tracker(vehicle,trackerId,innerMemory);
        return tracker;
    }







}

package cz.cvut.fel.omo.trackingSystem;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.jupiter.api.Order;

import static org.junit.Assert.*;

/**
 * Created by kuki on 22/09/2017.
 */
public class VehicleTest {
    Vehicle test = Params.newCar();
    String VINcode = Params.VINCode;
    String manufacturer = Params.manufacturer;
    int milage = Params.mileage;
    @Test
    @Order(1)
    public void getMileage_getsTheValueOfTheMilageParam_Returns0() {
        assertEquals(milage,test.getMileage());
    }

    @Test
    @Order(2)
    public void drive_changesMilage_makesMilage10() {
        int testNum = 10;
        assertEquals(milage,test.getMileage());
        test.drive(testNum);
        assertEquals(testNum,test.getMileage());
    }

    @Test
    @Order(3)
    public void getVINCode_getsTheStringOfThVINcodeParam_returns69(){
        assertEquals(VINcode,test.getVINCode());
    }

    @Test
    @Order(4)
    public void getManufacturer_getsTheStringOfTheManufacturerParam_RETURNSsimp() {
        assertEquals(manufacturer,test.getManufacturer());
    }

    @Test
    @Order(5)
    public void testToString_createsAStringContainingTheVinCodeAndManufacturer_RETURNSsimp69() {
        String expected = manufacturer + ", " + VINcode;
        assertEquals(expected,test.toString());
    }

    // IMPLEMENT ME, PLEASE!
}
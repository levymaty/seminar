package cz.cvut.fel.omo.trackingSystem;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static org.junit.Assert.*;

/**
 * Created by kuki on 22/09/2017.
 */
public class GPSTrackingSystemTest {
    List<Vehicle> vehicles = new ArrayList<>();
    GpsTrackingSystem test = new GpsTrackingSystem();
    @Test
    public void attachTrackingDevices_AttachesTrackingDevicesToCarsInList_AssertsTrue() {
        test = new GpsTrackingSystem();
        vehicles.add(Params.newCar());
        String expect = "—– GPS Tracking system: Monthly report —–\n" +
                "\n" +
                "Currently active devices:\n" +
                "\n" +
                "This month traveled distance: 0 Km.\n";
        String result = rederectconsoleoutputtostring();
        System.out.println(expect);
        System.out.println(result);
            assertTrue(expect.equals(result.toString()));
            test.attachTrackingDevices(vehicles);
            expect = "—– GPS Tracking system: Monthly report —–\n" +
                "\n" +
                "Currently active devices:\n" +
                "\n" +
                "Tracker_0, attached to SIMP, 69\n"
                +
                "\n"
                +
                "This month traveled distance: 0 Km.\n";
    }

    @Test
    public void generateMonthlyReport_PrintsOutAReport_AssertsTrue() {
        List<Tracker> trackers = new ArrayList<>();
        Tracker tracker = Params.newTracker();
        trackers.add(tracker);
        test = new GpsTrackingSystem(trackers);
        vehicles.add(Params.newCar());
        String expect = "—– GPS Tracking system: Monthly report —–\n" +
                "\n" +
                "Currently active devices:\n" +
                "\n" +
                "Tracker_666, attached to SIMP, 69\n" +
                "\n" +
                "This month traveled distance: 0 Km.\n";
        assertEquals(expect,rederectconsoleoutputtostring());
    }
    // IMPLEMENT ME, PLEASE!

    public String rederectconsoleoutputtostring(){
        PrintStream old = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        System.setOut(ps);
        test.generateMonthlyReport();
        System.out.flush();
        System.setOut(old);
        return baos.toString();
    }
}